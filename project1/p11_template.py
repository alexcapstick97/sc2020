""" Your college id here:
    Template code for part 1, contains 4 functions:
    newSort, merge: codes for part 1.1
    time_newSort: to be completed for part 1.1
    findTrough: to be completed for part 1.2
"""


def newSort(X,k=0):
    """Given an unsorted list of integers, X,
        sort list and return sorted list
    """

    n = len(X)
    if n==1:
        return X
    elif n<=k:
        for i in range(n-1):
            ind_min = i
            for j in range(i+1,n):
                if X[j]<X[ind_min]:
                    ind_min = j
            X[i],X[ind_min] = X[ind_min],X[i]
        return X
    else:
        L = newSort(X[:n//2],k)
        R = newSort(X[n//2:],k)
        return merge(L,R)


def merge(L,R):
    """Merge 2 sorted lists provided as input
    into a single sorted list
    """
    M = [] #Merged list, initially empty
    indL,indR = 0,0 #start indices
    nL,nR = len(L),len(R)

    #Add one element to M per iteration until an entire sublist
    #has been added
    for i in range(nL+nR):
        if L[indL]<R[indR]:
            M.append(L[indL])
            indL = indL + 1
            if indL>=nL:
                M.extend(R[indR:])
                break
        else:
            M.append(R[indR])
            indR = indR + 1
            if indR>=nR:
                M.extend(L[indL:])
                break
    return M


def time_newSort(inputs=None):
    """Analyze performance of newSort
    Use variables inputs and outputs if/as needed
    """


    outputs=None
    return outputs


def findTrough(L):
    """Find and return a location of a trough in L
    """
    i = None #should be modified

    return i


if __name__=='__main__':
    inputs=None
    outputs=time_newSort(inputs)
